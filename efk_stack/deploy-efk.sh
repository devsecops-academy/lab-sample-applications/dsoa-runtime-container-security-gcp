#!/bin/bash

kubectl apply -f "https://raw.githubusercontent.com/GoogleCloudPlatform/marketplace-k8s-app-tools/master/crd/app-crd.yaml"


export APP_INSTANCE_NAME=elastic-logging-1
export NAMESPACE=default
export ELASTICSEARCH_REPLICAS=2
export METRICS_EXPORTER_ENABLED=false

TAG=6.3.2-20200306-133130
export IMAGE_ELASTICSEARCH="gcr.io/cloud-marketplace/google/elastic-gke-logging"
export IMAGE_KIBANA="gcr.io/cloud-marketplace/google/elastic-gke-logging/kibana:${TAG}"
export IMAGE_FLUENTD="gcr.io/cloud-marketplace/google/elastic-gke-logging/fluentd:${TAG}"
export IMAGE_INIT="gcr.io/cloud-marketplace/google/elastic-gke-logging/ubuntu16_04:${TAG}"
export IMAGE_METRICS_EXPORTER="marketplace.gcr.io/google/elastic-gke-logging/prometheus-to-sd"

export FLUENTD_SERVICE_ACCOUNT="$APP_INSTANCE_NAME-fluentdserviceaccount"
kubectl create serviceaccount $FLUENTD_SERVICE_ACCOUNT --namespace default
kubectl create clusterrole $FLUENTD_SERVICE_ACCOUNT-role --verb=get,list,watch --resource=pods,namespaces
kubectl create clusterrolebinding $FLUENTD_SERVICE_ACCOUNT-rule --clusterrole=$FLUENTD_SERVICE_ACCOUNT-role --serviceaccount=default:$FLUENTD_SERVICE_ACCOUNT

kubectl apply -f "efk_manifest.yaml" --namespace default
