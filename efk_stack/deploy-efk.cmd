kubectl apply -f "https://raw.githubusercontent.com/GoogleCloudPlatform/marketplace-k8s-app-tools/master/crd/app-crd.yaml"

SET APP_INSTANCE_NAME="elastic-logging-1"
SET NAMESPACE="default"
SET ELASTICSEARCH_REPLICAS="2"
SET METRICS_SETER_ENABLED="false"

SET TAG="6.3.2-20200306-133130"
SET IMAGE_ELASTICSEARCH="gcr.io/cloud-marketplace/google/elastic-gke-logging"
SET IMAGE_KIBANA="gcr.io/cloud-marketplace/google/elastic-gke-logging/kibana:%TAG%"
SET IMAGE_FLUENTD="gcr.io/cloud-marketplace/google/elastic-gke-logging/fluentd:%TAG%"
SET IMAGE_INIT="gcr.io/cloud-marketplace/google/elastic-gke-logging/ubuntu16_04:%TAG%"
SET IMAGE_METRICS_SETER="marketplace.gcr.io/google/elastic-gke-logging/prometheus-to-sd"

SET FLUENTD_SERVICE_ACCOUNT="%APP_INSTANCE_NAME%-fluentdserviceaccount"
kubectl create serviceaccount %FLUENTD_SERVICE_ACCOUNT% --namespace default
kubectl create clusterrole %FLUENTD_SERVICE_ACCOUNT%-role --verb=get,list,watch --resource=pods,namespaces
kubectl create clusterrolebinding %FLUENTD_SERVICE_ACCOUNT%-rule --clusterrole=%FLUENTD_SERVICE_ACCOUNT%-role --serviceaccount=default:%FLUENTD_SERVICE_ACCOUNT%

kubectl apply -f "efk_manifest.yaml" --namespace default
